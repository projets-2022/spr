#ifndef BCM2835WRAPPER_H
#define BCM2835WRAPPER_H

#include <QtGlobal>

#if defined(Q_PROCESSOR_ARM)
#include <bcm2835.h>
#else
#define HIGH 0x1

#define LOW  0x0

typedef enum
{
    BCM2835_I2C_REASON_OK            = 0x00,
    BCM2835_I2C_REASON_ERROR_NACK    = 0x01,
    BCM2835_I2C_REASON_ERROR_CLKT    = 0x02,
    BCM2835_I2C_REASON_ERROR_DATA    = 0x04
} bcm2835I2CReasonCodes;

typedef enum
{
    BCM2835_SPI_MODE0 = 0,
    BCM2835_SPI_MODE1 = 1,
    BCM2835_SPI_MODE2 = 2,
    BCM2835_SPI_MODE3 = 3
} bcm2835SPIMode;

typedef enum
{
    BCM2835_SPI_CS0 = 0,
    BCM2835_SPI_CS1 = 1,
    BCM2835_SPI_CS2 = 2,
    BCM2835_SPI_CS_NONE = 3
} bcm2835SPIChipSelect;

typedef enum
{
    BCM2835_I2C_CLOCK_DIVIDER_2500   = 2500,
    BCM2835_I2C_CLOCK_DIVIDER_626    = 626,
    BCM2835_I2C_CLOCK_DIVIDER_150    = 150,
    BCM2835_I2C_CLOCK_DIVIDER_148    = 148
} bcm2835I2CClockDivider;

typedef enum
{
    BCM2835_SPI_CLOCK_DIVIDER_65536 = 0,
    BCM2835_SPI_CLOCK_DIVIDER_32768 = 32768,
    BCM2835_SPI_CLOCK_DIVIDER_16384 = 16384,
    BCM2835_SPI_CLOCK_DIVIDER_8192  = 8192,
    BCM2835_SPI_CLOCK_DIVIDER_4096  = 4096,
    BCM2835_SPI_CLOCK_DIVIDER_2048  = 2048,
    BCM2835_SPI_CLOCK_DIVIDER_1024  = 1024,
    BCM2835_SPI_CLOCK_DIVIDER_512   = 512,
    BCM2835_SPI_CLOCK_DIVIDER_256   = 256,
    BCM2835_SPI_CLOCK_DIVIDER_128   = 128,
    BCM2835_SPI_CLOCK_DIVIDER_64    = 64,
    BCM2835_SPI_CLOCK_DIVIDER_32    = 32,
    BCM2835_SPI_CLOCK_DIVIDER_16    = 16,
    BCM2835_SPI_CLOCK_DIVIDER_8     = 8,
    BCM2835_SPI_CLOCK_DIVIDER_4     = 4,
    BCM2835_SPI_CLOCK_DIVIDER_2     = 2,
    BCM2835_SPI_CLOCK_DIVIDER_1     = 1
} bcm2835SPIClockDivider;

typedef enum
{
    RPI_GPIO_P1_03        =  0,
    RPI_GPIO_P1_05        =  1,
    RPI_GPIO_P1_07        =  4,
    RPI_GPIO_P1_08        = 14,
    RPI_GPIO_P1_10        = 15,
    RPI_GPIO_P1_11        = 17,
    RPI_GPIO_P1_12        = 18,
    RPI_GPIO_P1_13        = 21,
    RPI_GPIO_P1_15        = 22,
    RPI_GPIO_P1_16        = 23,
    RPI_GPIO_P1_18        = 24,
    RPI_GPIO_P1_19        = 10,
    RPI_GPIO_P1_21        =  9,
    RPI_GPIO_P1_22        = 25,
    RPI_GPIO_P1_23        = 11,
    RPI_GPIO_P1_24        =  8,
    RPI_GPIO_P1_26        =  7,
    /* RPi Version 2 */
    RPI_V2_GPIO_P1_03     =  2,
    RPI_V2_GPIO_P1_05     =  3,
    RPI_V2_GPIO_P1_07     =  4,
    RPI_V2_GPIO_P1_08     = 14,
    RPI_V2_GPIO_P1_10     = 15,
    RPI_V2_GPIO_P1_11     = 17,
    RPI_V2_GPIO_P1_12     = 18,
    RPI_V2_GPIO_P1_13     = 27,
    RPI_V2_GPIO_P1_15     = 22,
    RPI_V2_GPIO_P1_16     = 23,
    RPI_V2_GPIO_P1_18     = 24,
    RPI_V2_GPIO_P1_19     = 10,
    RPI_V2_GPIO_P1_21     =  9,
    RPI_V2_GPIO_P1_22     = 25,
    RPI_V2_GPIO_P1_23     = 11,
    RPI_V2_GPIO_P1_24     =  8,
    RPI_V2_GPIO_P1_26     =  7,
    RPI_V2_GPIO_P1_29     =  5,
    RPI_V2_GPIO_P1_31     =  6,
    RPI_V2_GPIO_P1_32     = 12,
    RPI_V2_GPIO_P1_33     = 13,
    RPI_V2_GPIO_P1_35     = 19,
    RPI_V2_GPIO_P1_36     = 16,
    RPI_V2_GPIO_P1_37     = 26,
    RPI_V2_GPIO_P1_38     = 20,
    RPI_V2_GPIO_P1_40     = 21,
    /* RPi Version 2, new plug P5 */
    RPI_V2_GPIO_P5_03     = 28,
    RPI_V2_GPIO_P5_04     = 29,
    RPI_V2_GPIO_P5_05     = 30,
    RPI_V2_GPIO_P5_06     = 31,
    /* RPi B+ J8 header, also RPi 2 40 pin GPIO header */
    RPI_BPLUS_GPIO_J8_03     =  2,
    RPI_BPLUS_GPIO_J8_05     =  3,
    RPI_BPLUS_GPIO_J8_07     =  4,
    RPI_BPLUS_GPIO_J8_08     = 14,
    RPI_BPLUS_GPIO_J8_10     = 15,
    RPI_BPLUS_GPIO_J8_11     = 17,
    RPI_BPLUS_GPIO_J8_12     = 18,
    RPI_BPLUS_GPIO_J8_13     = 27,
    RPI_BPLUS_GPIO_J8_15     = 22,
    RPI_BPLUS_GPIO_J8_16     = 23,
    RPI_BPLUS_GPIO_J8_18     = 24,
    RPI_BPLUS_GPIO_J8_19     = 10,
    RPI_BPLUS_GPIO_J8_21     =  9,
    RPI_BPLUS_GPIO_J8_22     = 25,
    RPI_BPLUS_GPIO_J8_23     = 11,
    RPI_BPLUS_GPIO_J8_24     =  8,
    RPI_BPLUS_GPIO_J8_26     =  7,
    RPI_BPLUS_GPIO_J8_29     =  5,
    RPI_BPLUS_GPIO_J8_31     =  6,
    RPI_BPLUS_GPIO_J8_32     = 12,
    RPI_BPLUS_GPIO_J8_33     = 13,
    RPI_BPLUS_GPIO_J8_35     = 19,
    RPI_BPLUS_GPIO_J8_36     = 16,
    RPI_BPLUS_GPIO_J8_37     = 26,
    RPI_BPLUS_GPIO_J8_38     = 20,
    RPI_BPLUS_GPIO_J8_40     = 21
} RPiGPIOPin;
#endif

class Bcm2835Wrapper
{
public:
    static Bcm2835Wrapper& getInstance();
    virtual ~Bcm2835Wrapper();

    // Rendre inaccessible les constructeurs de copie/déplacement
    // et opérateurs d'affectation/déplacement
    Bcm2835Wrapper(Bcm2835Wrapper const&) =delete;
    Bcm2835Wrapper(Bcm2835Wrapper&&) = delete;
    Bcm2835Wrapper& operator=(Bcm2835Wrapper const&) = delete;
    Bcm2835Wrapper operator=(Bcm2835Wrapper&&) = delete;

    typedef enum {LOGIC0=LOW, LOGIC1=HIGH} level_t;

    // Module GPIO
    typedef enum {INPUT, OUTPUT} gpio_mode_t;
    typedef enum {OFF, DOWN, UP} gpio_pud_t;
    void gpio_fsel(uint8_t pin, gpio_mode_t mode);
    void gpio_set_pud(uint8_t pin, gpio_pud_t pud);
    void gpio_clr(uint8_t pin);
    void gpio_set(uint8_t pin);
    uint8_t gpio_lev(uint8_t pin);

    // Module I2C
    typedef enum {STANDARD, FULL, FAST} i2c_clock_t;
    typedef enum {SUCCESS=BCM2835_I2C_REASON_OK
                  , NAK_ERR=BCM2835_I2C_REASON_ERROR_NACK
                  , CLK_STRETCH_ERR=BCM2835_I2C_REASON_ERROR_CLKT
                  , DATA_ERR=BCM2835_I2C_REASON_ERROR_DATA
                 } i2c_err_t;
    void i2c_setClock(i2c_clock_t clock);
    i2c_err_t i2c_write(uint8_t addr, uint8_t* buf, int len);
    i2c_err_t i2c_read(uint8_t addr, uint8_t* buf, int len);
    i2c_err_t i2c_write_read_rs(uint8_t addr, uint8_t* cmd, int cmdLen, uint8_t* buf, int bufLen);

    // Module SPI
    typedef enum {SPI_SCLK_100KHZ=100000, SPI_SCLK_1MHz=1000000} spi_clock_t;
    void spi_setClock(spi_clock_t clock);
    typedef enum : uint8_t {SPI_MODE_0=BCM2835_SPI_MODE0
        , SPI_MODE_1=BCM2835_SPI_MODE1
        , SPI_MODE_2=BCM2835_SPI_MODE2
        , SPI_MODE_3=BCM2835_SPI_MODE3
        } spi_mode_t;
    void spi_setDataMode(spi_mode_t mode);
    typedef enum  : uint8_t {SPI_CS0 = BCM2835_SPI_CS0
        , SPI_CS1 = BCM2835_SPI_CS1
        } spi_cs_t;
    void spi_chipSelect(spi_cs_t cs);
    void spi_setChipSelectPolarity(spi_cs_t cs, level_t activeLevel);
    void spi_writenb(uint8_t * tbuf, int len);
    void spi_transfernb(uint8_t * tbuf, uint8_t * rbuf, int len);

    // Module PWM
    void pwm_set_clock (uint32_t divisor);
    void pwm_set_mode (uint8_t channel, uint8_t markspace, uint8_t enabled);
    void pwm_set_range (uint8_t channel, uint32_t range);
    void pwm_set_data (uint8_t channel, uint32_t data);

private:
    static const uint16_t _I2C_CLK_DIVIDER = BCM2835_I2C_CLOCK_DIVIDER_2500;
    static const uint16_t _SPI_CLK_DIVIDER = BCM2835_SPI_CLOCK_DIVIDER_64;
    Bcm2835Wrapper();
    uint16_t _getCoreClkMHz();
    uint16_t _coreFreqMHz;
};

#endif // BCM2835WRAPPER_H
