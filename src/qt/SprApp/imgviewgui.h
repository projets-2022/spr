#ifndef VIEWIMAGE_H
#define VIEWIMAGE_H

#include <QDialog>

/*
 * Classe ViewImage
 * Gère une deuxième fenêtre pour afficher les images
 */

namespace Ui {
class ImgViewGUI;
}

class ImgViewGUI : public QDialog
{
    Q_OBJECT

public:
    ImgViewGUI(QWidget *parent = nullptr);
    ImgViewGUI(QImage&, int);
    ~ImgViewGUI();

private slots:
    void on_pbOk_clicked();
    void on_pbCompare_clicked();

private:
    Ui::ImgViewGUI *ui;
    //QImage m_image;
    int m_idImage;

signals:
    int sig_compare(int);
    void sig_close(int);
};

#endif // VIEWIMAGE_H
