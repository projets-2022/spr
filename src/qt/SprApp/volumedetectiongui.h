#ifndef VOLUMEDETECTIONGUI_H
#define VOLUMEDETECTIONGUI_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class VolumeDetectionGUI;
}

class VolumeDetectionGUI : public QDialog
{
    Q_OBJECT

public:
    explicit VolumeDetectionGUI(QWidget *parent = nullptr);
    ~VolumeDetectionGUI();

private:
    Ui::VolumeDetectionGUI *ui;
    QTimer tmr;

private slots:
    void on_btnCancel_clicked();
    void onTmrTimeout();

signals:
    void quit();
    void canceled();
};

#endif // VOLUMEDETECTIONGUI_H
