#ifndef HCSR04_H
#define HCSR04_H

#include "volumesensor.h"
#include <QObject>
#include <QTimer>

#include "bcm2835wrapper.h"

class HCSR04 : public VolumeSensor
{
public:
    explicit HCSR04(QObject *parent = nullptr);

    /**
     * @brief getDistances
     * @param top
     * @param side
     * @param rear
     * @return 0 si OK
     * @return -1 si NOK
     *
     * Retourne les distances (en mm) mesurées par les 3 capteurs
     */
    int getDistances(float& top, float& side, float& rear) override;

private:
    Bcm2835Wrapper& _bcm;
    const uint8_t SIDE_SENSOR_I2C_ADDR = 0x08;
    const uint8_t TOP_SENSOR_I2C_ADDR = 0x09;
    const uint8_t REAR_SENSOR_I2C_ADDR = 0x0A;
    QTimer _tmr;
    float _topDist;
    float _sideDist;
    float _rearDist;
    QVector<int> * sideSensor;
    QVector<int> * topSensor;
    QVector<int> * rearSensor;
    const int _NB_SAMPLES = 20; // nb échantillons sur lequel la distance moyenne sera évaluée
    const int _SENSOR_RESOLUTION = 5; // en mm

    float _computeMean(const QVector<int>& set);

private slots:
    void onTmrTimeout();

};

#endif // HCSR04_H
