#include "fakehcsr04.h"

FakeHCSR04::FakeHCSR04(QObject *parent) : VolumeSensor(parent)
{
    _ihm = new FakeHCSR04GUI();
    _ihm->show();
    _ihm->move(0,0);

    connect(_ihm, &FakeHCSR04GUI::distancesChanged, this, &FakeHCSR04::onDistancesChanged);
}

FakeHCSR04::~FakeHCSR04()
{
    delete _ihm;
}

int FakeHCSR04::getDistances(float &top, float &side, float &rear)
{
    top = _topDist;
    side = _sideDist;
    rear = _rearDist;

    return 0;
}

void FakeHCSR04::onDistancesChanged(int top, int side, int rear)
{
    _topDist = static_cast<float>(top);
    _sideDist = static_cast<float>(side);
    _rearDist = static_cast<float>(rear);

}
