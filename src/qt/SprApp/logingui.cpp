#include <QThread>

#include "logingui.h"
#include "ui_logingui.h"

LoginGUI::LoginGUI(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::LoginGUI)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowModality(Qt::WindowModal);

    ui->lblError->setVisible(false);

    setEnabled(true);

    ui->ledtUsername->setFocus();

}

LoginGUI::~LoginGUI()
{
    delete ui;
}

void LoginGUI::showError()
{
    ui->lblError->setVisible(true);
    QCoreApplication::processEvents();
    QThread::sleep(1);
    ui->lblError->setVisible(false);
    ui->ledtPassword->clear();
    ui->ledtUsername->clear();
    ui->ledtUsername->setFocus();
}

void LoginGUI::on_btnLogin_clicked()
{
    emit submit(ui->ledtUsername->text(), ui->ledtPassword->text());
}


void LoginGUI::on_btnQuit_clicked()
{
    emit quit();
}

