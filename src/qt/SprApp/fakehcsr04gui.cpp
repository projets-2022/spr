#include "fakehcsr04gui.h"
#include "ui_fakehcsr04GUI.h"

FakeHCSR04GUI::FakeHCSR04GUI(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FakeHCSR04GUI)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window | Qt::WindowStaysOnTopHint);
    setWindowModality(Qt::WindowModal);

    connect(ui->sldrRearSensor, &QSlider::valueChanged, this, &FakeHCSR04GUI::onValueChanged);
    connect(ui->sldrTopSensor, &QSlider::valueChanged, this, &FakeHCSR04GUI::onValueChanged);
    connect(ui->sldrSideSensor, &QSlider::valueChanged, this, &FakeHCSR04GUI::onValueChanged);
}

FakeHCSR04GUI::~FakeHCSR04GUI()
{
    delete ui;
}

void FakeHCSR04GUI::onValueChanged(int val)
{
    ui->ledtTopSensor->setText(QString::number(ui->sldrTopSensor->value()));
    ui->ledtSideSensor->setText(QString::number(ui->sldrSideSensor->value()));
    ui->ledtRearSensor->setText(QString::number(ui->sldrRearSensor->value()));
    emit distancesChanged(ui->sldrTopSensor->value(), ui->sldrSideSensor->value(), ui->sldrRearSensor->value());
}

