#ifndef FAKEHCSR04GUI_H
#define FAKEHCSR04GUI_H

#include <QDialog>

namespace Ui {
class FakeHCSR04GUI;
}

class FakeHCSR04GUI : public QDialog
{
    Q_OBJECT

public:
    explicit FakeHCSR04GUI(QWidget *parent = nullptr);
    ~FakeHCSR04GUI();

private:
    Ui::FakeHCSR04GUI *ui;

signals:
    void distancesChanged(int top, int side, int rear);

private slots:
    void onValueChanged(int val);
};

#endif // FAKEHCSR04GUI_H
