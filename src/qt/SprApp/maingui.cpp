#include <QThread>
#include <QMessageBox>
#include <QtGlobal>
#include <QDebug>
#include "maingui.h"
#include "ui_maingui.h"
#include "imgviewgui.h"

// Décommenter pour activer l'authentification
//#define LOGIN_ENABLED

MainGUI::MainGUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainGUI)
{
    ui->setupUi(this);

    // Mise en place des connexions signaux-slots :
    // 1/ Mode de sélection de caméra (auto vs. manuel)
    connect(ui->btnSelCameraAuto, &QAbstractButton::clicked, this, &MainGUI::onCameraModeChanged);
    connect(ui->btnSelCameraManual, &QAbstractButton::clicked, this, &MainGUI::onCameraModeChanged);
    // 2/ Bouton de sléction de caméra (grand angle vs. angle reserré)
    connect(ui->btnSwitchCamera, &QAbstractButton::clicked, this, &MainGUI::onBtnSwitchCameraClicked);
    // 3/ Mode de sélection de commande d'éclairage (auto vs. manuel)
    connect(ui->btnLightAuto, &QAbstractButton::clicked, this, &MainGUI::onLightModeChanged);
    connect(ui->btnLightManual, &QAbstractButton::clicked, this, &MainGUI::onLightModeChanged);
    // 4/ Mode de sélection de commande moteur (auto vs. manuel)
    connect(ui->btnMotorAuto, &QAbstractButton::clicked, this, &MainGUI::onMotorModeChanged);
    connect(ui->btnMotorManual, &QAbstractButton::clicked, this, &MainGUI::onMotorModeChanged);
    // 5/ Déclenchement caméra
    connect(ui->btnShoot, &QAbstractButton::clicked, this, &MainGUI::onBtnShootClicked);
    // 6/ Affichage plein écran des clichés
    connect(ui->lblFrontFace, &ClickableLabel::clicked, this, &MainGUI::onFaceClicked);
    connect(ui->lblLeftFace, &ClickableLabel::clicked, this, &MainGUI::onFaceClicked);
    connect(ui->lblRearFace, &ClickableLabel::clicked, this, &MainGUI::onFaceClicked);
    connect(ui->lblRightFace, &ClickableLabel::clicked, this, &MainGUI::onFaceClicked);

    // Configuration initiale du bouton de commutation de caméra :
    // 1/ Spécifie que le bouton de commutation de caméra garde le même espace lorsque caché
    QSizePolicy sp_retain = ui->btnSwitchCamera->sizePolicy();
    sp_retain.setRetainSizeWhenHidden(true);
    ui->btnSwitchCamera->setSizePolicy(sp_retain);
    // 2/ Rend invisible le bouton
    ui->btnSwitchCamera->setVisible(false);

    // Configure l'ihm de commande de l'élairage en mode "Automatique"
    ui->stackLightIntensity->setCurrentIndex(0);

    // Configure l'ihm de commande moteur en mode "Automatique"
    ui->stackMotor->setCurrentIndex(0);

    _cam = new Camera();

    // Sélectionne la caméra en accord avec celle activée par défaut sur l'ihmR
    _cam->select(Camera::NARROW);

    // Désactive l'ihm principale tant que l'authentification n'est pas faite
    setEnabled(false);

    // Traitement de l'authentification :
#ifdef LOGIN_ENABLED
    // SI Authentification activée ALORS gère l'authentification
    _login = new LoginGUI(this);
    connect(_login, &LoginGUI::submit, this, &MainGUI::onLoginSubmit);
    connect(_login, &LoginGUI::quit, this, &QApplication::quit);
    _login->show();
#else
    // SINON Active directement l'ihm principale
    setEnabled(true);
#endif

    _faces.append(qMakePair(Camera::FRONT, ui->lblFrontFace));
    _faces.append(qMakePair(Camera::RIGHT, ui->lblRightFace));
    _faces.append(qMakePair(Camera::BACK, ui->lblRearFace));
    _faces.append(qMakePair(Camera::LEFT, ui->lblLeftFace));
}

MainGUI::~MainGUI()
{
    delete ui;
}

/*
###########
## SLOTS ##
###########
*/

void MainGUI::onLoginSubmit(QString userName, QString password)
{
    // SI Authentification réussie ALORS
    if((userName == "user" && password == "user")
            || (userName == "admin" && password == "admin")
            ) {
        // Ferme fenêtre login
        _login->hide();
        delete _login;
        // Active l'ihm principale
        setEnabled(true);
    // SINON
    } else {
        // Affiche message d'erreur d'authentification
        _login->showError();
    }
}

void MainGUI::onCameraModeChanged()
{
    QObject * sigSrc = sender();
    if(sigSrc == ui->btnSelCameraAuto) {
        ui->btnSwitchCamera->setVisible(false);
//#if defined Q_OS_WIN
//        _volume = new FakeHCSR04();
//#else
//        _volume = new HCSR04();
//#endif

    } else if (sigSrc == ui->btnSelCameraManual) {
        ui->btnSwitchCamera->setVisible(true);

//        delete _volume;
    } else {
        /* non géré */
    }
}

void MainGUI::onLightModeChanged()
{
    QObject * sigSrc = sender();
    if(sigSrc == ui->btnLightAuto) {
        ui->stackLightIntensity->setCurrentIndex(0);
        on_btnLightAuto_clicked();
    } else if (sigSrc == ui->btnLightManual) {
        ui->stackLightIntensity->setCurrentIndex(1);
    } else {
        /* non géré */
    }
}

void MainGUI::onMotorModeChanged()
{
    QObject * sigSrc = sender();
    if(sigSrc == ui->btnMotorAuto) {
        ui->stackMotor->setCurrentIndex(0);
    } else if (sigSrc == ui->btnMotorManual) {
        ui->stackMotor->setCurrentIndex(1);
    } else {
        /* non géré */
    }
}

void MainGUI::onBtnSwitchCameraClicked()
{
    // SI caméra grand angle actuellement sélectionnée ALORS
    if( ui->lblWideAngle->isEnabled()) {
        // Sélectionne physiquement la caméra "angle reserré"
        _cam->select(Camera::NARROW);
        // Active l'icône de la caméra "angle reserré" sur l'ihm
        ui->lblNarrowAngle->setFrameShape(QFrame::Box);
        ui->lblNarrowAngle->setFrameShadow(QFrame::Raised);
        ui->lblNarrowAngle->setLineWidth(2);
        ui->lblNarrowAngle->setMidLineWidth(0);
        ui->lblNarrowAngle->setAutoFillBackground(false);
        ui->lblNarrowAngle->setStyleSheet(QString::fromUtf8("#lblNarrowAngle {\nbackground-color:azure;\n}"));
        ui->lblNarrowAngle->setEnabled(true);
        // Désactive l'icône de la caméra "grand angle" sur l'ihm
        ui->lblWideAngle->setFrameShape(QFrame::NoFrame);
        ui->lblWideAngle->setStyleSheet(nullptr);
        ui->lblWideAngle->setEnabled(false);
    } else {
        // Sélectionne physiquement la caméra "grand angle"
        _cam->select(Camera::WIDE);
        // Active l'icône de la caméra "grand angle" sur l'ihm
        ui->lblWideAngle->setFrameShape(QFrame::Box);
        ui->lblWideAngle->setFrameShadow(QFrame::Raised);
        ui->lblWideAngle->setLineWidth(2);
        ui->lblWideAngle->setMidLineWidth(0);
        ui->lblWideAngle->setAutoFillBackground(false);
        ui->lblWideAngle->setStyleSheet(QString::fromUtf8("#lblWideAngle {\nbackground-color:azure;\n}"));
        ui->lblWideAngle->setEnabled(true);
        // Désactive l'icône de la caméra "angle reserré" sur l'ihm
        ui->lblNarrowAngle->setFrameShape(QFrame::NoFrame);
        ui->lblNarrowAngle->setStyleSheet(nullptr);
        ui->lblNarrowAngle->setEnabled(false);
    }
}

void MainGUI::onBtnShootClicked()
{
    QImage image;
    int angle = 0;

    if(ui->btnSelCameraAuto->isChecked()) {
#if defined Q_OS_WIN
        _volume = new FakeHCSR04();
        //_volume = new HCSR04();
#else
        _volume = new HCSR04();
#endif
        _waitVolumeDetectionDlg = new VolumeDetectionGUI();
        connect(_waitVolumeDetectionDlg
                , &VolumeDetectionGUI::canceled
                , this
                , &MainGUI::onVolumeDetectionCanceled
                );
        connect(_waitVolumeDetectionDlg
                , &VolumeDetectionGUI::quit
                , this
                , &MainGUI::onVolumeDetectionQuit
                );
        setEnabled(false);
        _waitVolumeDetectionDlg->setWindowModality(Qt::WindowModal);
        _waitVolumeDetectionDlg->show();
        _waitVolumeDetectionDlg->exec();
        delete _waitVolumeDetectionDlg;
        setEnabled(true);
    }

    if(ui->btnMotorAuto->isChecked()) {
        // Effacer les captures déjà présentes sur l'ihm
        for(auto& f : _faces) {
            f.second->clear();
        }

        for(auto& f : _faces) {


            if (ui->btnLightAuto->isChecked()) {
                double lux = _light.getLux();
                ui->ledtLux->setText(QString::number(lux));

                uint8_t level = _light.setBrightness();
                ui->ledtLightIntensity->setText(QString::number(level));
            }

            _cam->takeCapture(f.first);
            image = _cam->getCapture(f.first);
            f.second->setPixmap(QPixmap::fromImage(image));
            f.second->setScaledContents(true);
            ui->progressShoot->setValue(angle);
            angle += 90;
#if defined Q_OS_WIN
            QThread::sleep(1);
#else
            _motor.quaterTurnR();
#endif
            ui->progressShoot->setValue(angle);
        }

    } else {
        int idx = ui->cbxFace->currentIndex();
        auto& f = _faces[ idx ];
        f.second->clear();
        _cam->takeCapture(f.first);
        image = _cam->getCapture(f.first);
        f.second->setPixmap(QPixmap::fromImage(image));
        f.second->setScaledContents(true);
    }
}

void MainGUI::onFaceClicked()
{
    QObject * sigSrc = sender();
    ClickableLabel * lblFace = nullptr;
    Camera::face_t face;
    ImgViewGUI * view;
    QImage capture;


    if (sigSrc == ui->lblFrontFace && (ui->lblFrontFace->pixmap())) {
        lblFace = ui->lblFrontFace;
        face = Camera::FRONT;
    } else if (sigSrc == ui->lblRearFace) {
        lblFace = ui->lblRearFace;
        face = Camera::BACK;
    } else if (sigSrc == ui->lblLeftFace) {
        lblFace = ui->lblLeftFace;
        face = Camera::LEFT;
    } else if (sigSrc == ui->lblRightFace) {
        lblFace = ui->lblRightFace;
        face = Camera::RIGHT;
    } else {
        lblFace = ui->lblFrontFace;
        face = Camera::FRONT;
    }

//    if( !face->pixmap(Qt::ReturnByValue).isNull() ) {
    const QPixmap *pixmapPtr = lblFace->pixmap();
    if( pixmapPtr != nullptr ) {
        capture = _cam->getCapture(face);
        view = new ImgViewGUI(capture, 2);
        //connect(view, &ImgViewGUI::sig_compare, this, &MainGUI::CompareOneImage);
        //connect(view, &ImgViewGUI::sig_close, this, &Maingui::deleteWindow);
        view->show();
    }

}

void MainGUI::on_sldrBrightness_valueChanged(int value)
{
    _light.setBrightness(value);
}

void MainGUI::on_btnTurnLeft_clicked()
{
    _motor.quaterTurnL();
}

void MainGUI::on_btnTurnRight_clicked()
{
    _motor.quaterTurnR();
}

void MainGUI::on_btnArchive_clicked()
{
    _cam->archiveCaptures(ui->ledtBarCode->text(), nullptr);
}


void MainGUI::on_btnLightAuto_clicked()
{
    double lux = _light.getLux();
    ui->ledtLux->setText(QString::number(lux));

    uint8_t level = _light.setBrightness();
    ui->ledtLightIntensity->setText(QString::number(level));

}

void MainGUI::on_btnQuit_clicked()
{
    QMessageBox confirm(tr("Confirmation")
        , tr("Voulez-vous vraiment quitter l'application ?")
        , QMessageBox::Question
        , QMessageBox::Yes
        , QMessageBox::Cancel | QMessageBox::Default | QMessageBox::Escape
        , QMessageBox::NoButton
        , this
        );
    confirm.setButtonText(QMessageBox::Yes,tr("Oui"));
    confirm.setButtonText(QMessageBox::Cancel,tr("Ignorer"));

    if( confirm.exec() == QMessageBox::Yes) {
        exit(0);
    }
}

void MainGUI::onVolumeDetectionCanceled()
{
    _waitVolumeDetectionDlg->done(-1);

    if(_volume != nullptr) {
        delete _volume;
        _volume = nullptr;
    }
}

void MainGUI::onVolumeDetectionQuit()
{
    float top, side, rear;

    _waitVolumeDetectionDlg->done(0);

    _volume->getDistances(top, side, rear);
    qDebug() << "Distances mesurées : TOP = " << top << "mm / SIDE = " << side << "mm / REAR = " << rear << "mm";

    // Sélectionne la caméra la plus adaptée selon le volume du
    // produit à photographier
//    if( (x > 50) && (y > 50) && (z > 50)) {
    if( (top > 100)) {
        // Sélectionne physiquement la caméra "angle reserré"
        _cam->select(Camera::NARROW);
        // Active l'icône de la caméra "angle reserré" sur l'ihm
        ui->lblNarrowAngle->setFrameShape(QFrame::Box);
        ui->lblNarrowAngle->setFrameShadow(QFrame::Raised);
        ui->lblNarrowAngle->setLineWidth(2);
        ui->lblNarrowAngle->setMidLineWidth(0);
        ui->lblNarrowAngle->setAutoFillBackground(false);
        ui->lblNarrowAngle->setStyleSheet(QString::fromUtf8("#lblNarrowAngle {\nbackground-color:azure;\n}"));
        ui->lblNarrowAngle->setEnabled(true);
        // Désactive l'icône de la caméra "grand angle" sur l'ihm
        ui->lblWideAngle->setFrameShape(QFrame::NoFrame);
        ui->lblWideAngle->setStyleSheet(nullptr);
        ui->lblWideAngle->setEnabled(false);
    } else {
        // Sélectionne physiquement la caméra "grand angle"
        _cam->select(Camera::WIDE);
        // Active l'icône de la caméra "grand angle" sur l'ihm
        ui->lblWideAngle->setFrameShape(QFrame::Box);
        ui->lblWideAngle->setFrameShadow(QFrame::Raised);
        ui->lblWideAngle->setLineWidth(2);
        ui->lblWideAngle->setMidLineWidth(0);
        ui->lblWideAngle->setAutoFillBackground(false);
        ui->lblWideAngle->setStyleSheet(QString::fromUtf8("#lblWideAngle {\nbackground-color:azure;\n}"));
        ui->lblWideAngle->setEnabled(true);
        // Désactive l'icône de la caméra "angle reserré" sur l'ihm
        ui->lblNarrowAngle->setFrameShape(QFrame::NoFrame);
        ui->lblNarrowAngle->setStyleSheet(nullptr);
        ui->lblNarrowAngle->setEnabled(false);
    }

    if(_volume != nullptr) {
        delete _volume;
        _volume = nullptr;
    }
}

