#ifndef LOGINGUI_H
#define LOGINGUI_H

#include <QFrame>

namespace Ui {
class LoginGUI;
}

class LoginGUI : public QFrame
{
    Q_OBJECT

public:
    explicit LoginGUI(QWidget *parent = nullptr);
    ~LoginGUI();
    void showError();

private slots:
    void on_btnLogin_clicked();
    void on_btnQuit_clicked();

private:
    Ui::LoginGUI *ui;

signals:
    void submit(QString userName, QString password);
    void quit();
};

#endif // LOGINGUI_H
