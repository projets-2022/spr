#ifndef VOLUMESENSOR_H
#define VOLUMESENSOR_H

#include <QObject>

class VolumeSensor : public QObject
{
    Q_OBJECT
public:
    explicit VolumeSensor(QObject *parent = nullptr);

    virtual int getDistances(float& top, float& side, float& rear) = 0;

protected:

signals:
};

#endif // VOLUMESENSOR_H
