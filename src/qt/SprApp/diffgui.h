#ifndef COMPARE_H
#define COMPARE_H

#include "diffloggui.h"

#include <QDebug>
#include <QDirIterator>
#include <QMainWindow>
#include <QVector>

namespace Ui {
class DiffGUI;
}

class DiffGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit DiffGUI(QWidget *parent = nullptr);
    DiffGUI(QString, int);
    ~DiffGUI();

private slots:
    void on_pbOK_clicked();

    void on_pbREPORT_clicked();

    void on_pbNEXT_clicked();

    void on_pbPREVIUS_clicked();

    void closeWindow();

private:
    Ui::DiffGUI *ui;
    QString m_cbar;
    QVector<QString> m_datePath;
    QVector<QString> m_date;
    QString m_DirectoryPath = "/home/pi/Desktop/stockPhotoShare/";
    QString m_path;

    void setImage(QString);

    QString getImage();
    QString getCurrentImage();
    void clearVoidElement();
    void clearCurrentDate();
    QString compareDate();
    QString addInitialZero();

    QString m_faces[4] = {"F", "R", "B", "L"};
    int m_idFace;
    QString m_curentDate;

    DiffLogGUI *m_windowReport;

signals:
    void sig_close(int);
};

#endif // COMPARE_H
