#include <stdio.h>
#include <QString>
#include <QDebug>

#include "stripled.h"

StripLed::StripLed()
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_init();
    bcm2835_gpio_fsel(12, BCM2835_GPIO_FSEL_ALT0);
    bcm2835_pwm_set_clock(BCM2835_PWM_CLOCK_DIVIDER_256);
    bcm2835_pwm_set_mode(_PWM_CHANNEL, 1, 1);
    bcm2835_pwm_set_range(_PWM_CHANNEL, _PWM_RANGE);
#endif
}

StripLed::~StripLed()
{
}

void StripLed::setBrightness(uint8_t level)
{
    if( level <= 100 ) {
#if defined(Q_PROCESSOR_ARM)
        uint32_t data = (level / 100.0) * _PWM_RANGE;
        bcm2835_pwm_set_data(_PWM_CHANNEL, data);
#endif
    } else {
        qDebug() << "Intensité d'éclairage hors limite : " << level << "%";
    }
}

