#ifndef PCA9629A_H
#define PCA9629A_H

#include <QObject>
#include "bcm2835wrapper.h"

/*
 * Classe PCA9629A
 * Gère tous les registres PCA9629A via une communication I2C
 * Utilise la Classe Bcm2835Wrapper
 */

class PCA9629A {
public:

    /** keyword to select direction of rotation */
    typedef enum {
        CW = 0, /**< Clockwise direction */
        CCW /**< ConterClockwise direction */
    } dir;

    typedef enum {
        UP,
        DOWN
    } ramp;

    PCA9629A(uint8_t addr = 0x20);
    virtual ~PCA9629A();

    void Reset();
    void start(dir dir);
    void stop();
    void setStep(dir dir, int stepCount);
    uint16_t setSpeed(dir dir, int pulsePerSecond);
    void setRamp(ramp ramp, uint8_t slope);
    bool isRunning();
    uint32_t getStepNumber();

private:

    const double  STEP_RESOLUTION = 1/(3e-6);

    typedef enum {
        MODE, /**< 0x00   Mode register                                           */
        WDTOI, /**< 0x01   WatchDog Time-Out Interval register                     */
        WDCNTL, /**< 0x02   WatchDog Control register                               */
        IO_CFG, /**< 0x03   I/O Configuration register                              */
        INTMODE, /**< 0x04   Interrupt Mode register                                 */
        MSK, /**< 0x05   Mask interrupt register                                 */
        INTSTAT, /**< 0x06   Interrupt Status register                               */
        IP, /**< 0x07   Input Port register                                     */
        INT_MTR_ACT, /**< 0x08   Interrupt motor action control register                 */
        EXTRASTEPS0, /**< 0x09   Extra steps count for INTP0 control register            */
        EXTRASTEPS1, /**< 0x0A   Extra steps count for INTP1 control register            */
        OP_CFG_PHS, /**< 0x0B   Output Port Configuration and Phase control register    */
        OP_STAT_TO, /**< 0x0C   Output state and time-out control register              */
        RUCNTL, /**< 0x0D   Ramp-up control register                                */
        RDCNTL, /**< 0x0E   Ramp-down control register                              */
        PMA, /**< 0x0F   Perform multiple of actions control register            */
        LOOPDLY_CW, /**< 0x10   Loop delay timer for CW to CCW control register         */
        LOOPDLY_CCW, /**< 0x11   Loop delay timer for CCW to CW control register         */
        CWSCOUNTL, /**< 0x12   Number of clockwise steps register (low byte)           */
        CWSCOUNTH, /**< 0x13   Number of clockwise steps register (high byte)          */
        CCWSCOUNTL, /**< 0x14   Number of counter-clockwise steps register (low byte)   */
        CCWSCOUNTH, /**< 0x15   Number of counter-clockwise steps register (high byte)  */
        CWPWL, /**< 0x16   Clockwise step pulse width register (low byte)          */
        CWPWH, /**< 0x17   Clockwise step pulse width register (high byte)         */
        CCWPWL, /**< 0x18   Counter-clockwise step pulse width register (low byte)  */
        CCWPWH, /**< 0x19   Counter-clockwise step pulse width register (high byte) */
        MCNTL, /**< 0x1A   Motor control register                                  */
        SUBADR1, /**< 0x1B   I2C-bus subaddress 1                                    */
        SUBADR2, /**< 0x1C   I2C-bus subaddress 2                                    */
        SUBADR3, /**< 0x1D   I2C-bus subaddress 3                                    */
        ALLCALLADR, /**< 0x1E   All Call I2C-bus address                                */
        STEPCOUNT0, /**< 0x1F   Step counter registers: STEPCOUNT0                      */
        STEPCOUNT1, /**< 0x20   Step counter registers: STEPCOUNT1                      */
        STEPCOUNT2, /**< 0x21   Step counter registers: STEPCOUNT2                      */
        STEPCOUNT3 /**< 0x22   Step counter registers: STEPCOUNT3                      */
    } reg8;

    /* register names for 2 bytes accessing */
    typedef enum {
        CWPW = CWPWL | 0x80, /**< Step pulse width for CW rotation */
        CCWPW = CCWPWL | 0x80, /**< Step pulse width for CCW rotation */
        CWSCOUNT = CWSCOUNTL | 0x80, /**< Number of steps CW */
        CCWSCOUNT = CCWSCOUNTL | 0x80, /**< Number of steps CCW */
    } reg16;

        /* register names for 4 bytes accessing */
    typedef enum {
        STEPCOUNT = STEPCOUNT0 | 0x80, /**< Step pulse width for CW rotation */
    } reg32;

    /* prescaler range setting */
    typedef enum {
        PRESCALER_FROM_40_TO_333333,    /*< Prescaler range from   3us(333333pps) to   24.576ms(40   pps) */
        PRESCALER_FROM_20_TO_166667,    /*< Prescaler range from   6us(166667pps) to   49.152ms(20   pps) */
        PRESCALER_FROM_10_TO_83333,     /*< Prescaler range from  12us( 83333pps) to   98.304ms(10   pps) */
        PRESCALER_FROM_5_TO_41667,      /*< Prescaler range from  24us( 41667pps) to  196.608ms( 5   pps) */
        PRESCALER_FROM_2_5_TO_20833,    /*< Prescaler range from  48us( 20833pps) to  393.216ms( 2.5 pps) */
        PRESCALER_FROM_1_27_TO_10416,   /*< Prescaler range from  96us( 10416pps) to  786.432ms( 1.27pps) */
        PRESCALER_FROM_0_64_TO_5208,    /*< Prescaler range from 192us(  5208pps) to 1572.864ms( 0.64pps) */
        PRESCALER_FROM_0_32_TO_2604,    /*< Prescaler range from 384us(  2604pps) to 3145.728ms( 0.32pps) */
    } prescaler_range;

    uint8_t m_addr;
    Bcm2835Wrapper& _bcm;
    bool m_isRunning;

    void initRegs();
    void write(reg8 reg, uint8_t val);
    void write(reg16 reg, uint16_t val);
    uint8_t read(reg8 reg);
    uint16_t read(reg16 reg);
    uint32_t read(reg32 reg);
};

#endif // PCA9629A_H
