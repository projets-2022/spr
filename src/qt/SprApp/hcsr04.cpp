#include <QDebug>

#include "hcsr04.h"

HCSR04::HCSR04(QObject *parent) : VolumeSensor(parent)
  , _bcm(Bcm2835Wrapper::getInstance())
{
    topSensor = new QVector<int>;
    rearSensor = new QVector<int>;
    sideSensor = new QVector<int>;

    connect(&_tmr, &QTimer::timeout, this, &HCSR04::onTmrTimeout);
    _tmr.setInterval(100);
    _tmr.start();
}

int HCSR04::getDistances(float &top, float &side, float &rear)
{
    int ret = -1;

    // A DECOMMENTER lorsque les 3 capteurs sont présents
    //if((_topDist > 0) && (_rearDist > 0) && (_sideDist > 0)) {
        top = _topDist;
        side = _sideDist;
        rear = _rearDist;
        ret = 0;
    //}


    return ret;
}

float HCSR04::_computeMean(const QVector<int> &set)
{
    float sum = 0.0;

    foreach(const int val, set) {
      sum += val;
    }

    return sum / set.length();
}

void HCSR04::onTmrTimeout()
{
    uint8_t rawSensor;
    Bcm2835Wrapper::i2c_err_t status;

    status = _bcm.i2c_read(SIDE_SENSOR_I2C_ADDR, &rawSensor, 1);
    if ( status == Bcm2835Wrapper::SUCCESS) {
        sideSensor->push_back(rawSensor * _SENSOR_RESOLUTION);
        if (sideSensor->length() >= _NB_SAMPLES) {
            sideSensor->removeFirst();
            _sideDist = _computeMean(*sideSensor);
        } else {
            _sideDist = sideSensor->last();
        }
    } else {
        qDebug() << "erreur lecture HC-SR04 à l'adresse " << SIDE_SENSOR_I2C_ADDR;
    }

    status = _bcm.i2c_read(TOP_SENSOR_I2C_ADDR, &rawSensor, 1);
    if ( status == Bcm2835Wrapper::SUCCESS) {
        topSensor->push_back(rawSensor * _SENSOR_RESOLUTION);
        if (topSensor->length() >= _NB_SAMPLES) {
            topSensor->removeFirst();
            _topDist = _computeMean(*topSensor);
            qDebug() << "Distance mesurée : TOP = " << _topDist << "(mean)";
        } else {
            _topDist = topSensor->last();
            qDebug() << "Distance mesurée : TOP = " << topSensor->last() << "(last)";
        }
    } else {
        qDebug() << "erreur lecture HC-SR04 à l'adresse " << TOP_SENSOR_I2C_ADDR;
    }

    status = _bcm.i2c_read(REAR_SENSOR_I2C_ADDR, &rawSensor, 1);
    if ( status == Bcm2835Wrapper::SUCCESS) {
        rearSensor->push_back(rawSensor * _SENSOR_RESOLUTION);
        if (rearSensor->length() >= _NB_SAMPLES) {
            rearSensor->removeFirst();
            _rearDist = _computeMean(*rearSensor);
        } else {
            _rearDist = rearSensor->last();
        }
    } else {
        qDebug() << "erreur lecture HC-SR04 à l'adresse " << REAR_SENSOR_I2C_ADDR;
    }


}
