#ifndef FAKEHCSR04_H
#define FAKEHCSR04_H

#include <QObject>

#include "volumesensor.h"
#include "fakehcsr04gui.h"

class FakeHCSR04 : public VolumeSensor
{
public:
    explicit FakeHCSR04(QObject *parent = nullptr);
    ~FakeHCSR04();
    int getDistances(float& top, float& side, float& rear) override;

private:
    FakeHCSR04GUI * _ihm;
    float _topDist;
    float _rearDist;
    float _sideDist;

private slots:
    void onDistancesChanged(int top, int side, int rear);
};

#endif // FAKEHCSR04_H
