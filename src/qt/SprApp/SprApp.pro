QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TRANSLATIONS    +=  gui_fr.ts

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    camera.cpp \
    fakehcsr04.cpp \
    fakehcsr04gui.cpp \
    hcsr04.cpp \
    lightsensor.cpp \
    main.cpp \
    maingui.cpp \
    pca9629a.cpp \
    motor.cpp \
    clickablelabel.cpp \
    bcm2835wrapper.cpp \
    diffgui.cpp \
    logingui.cpp \
    diffloggui.cpp \
    imgviewgui.cpp \
    smartlight.cpp \
    stripled.cpp \
    volumedetectiongui.cpp \
    volumesensor.cpp

HEADERS += \
    camera.h \
    fakehcsr04.h \
    fakehcsr04gui.h \
    hcsr04.h \
    lightsensor.h \
    maingui.h \
    pca9629a.h \
    motor.h \
    clickablelabel.h \
    bcm2835wrapper.h \
    diffgui.h \
    logingui.h \
    diffloggui.h \
    imgviewgui.h \
    smartlight.h \
    stripled.h \
    volumedetectiongui.h \
    volumesensor.h

FORMS += \
    diffgui.ui \
    fakehcsr04GUI.ui \
    logingui.ui \
    diffloggui.ui \
    imgviewgui.ui \
    maingui.ui \
    volumedetectiongui.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

unix:!macx: LIBS += -lbcm2835

RESOURCES += \
    rsc.qrc
