#include <QDate>

#include "diffgui.h"
#include "ui_diffgui.h"

DiffGUI::DiffGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DiffGUI)
{
    ui->setupUi(this);
    //ui->groupBox->move(this->geometry().center());
}

DiffGUI::DiffGUI(QString cbar, int face) :
    ui(new Ui::DiffGUI),
    m_idFace(face),
    m_cbar(cbar)
{
    ui->setupUi(this);
    m_curentDate = QDate::currentDate().toString(QString("dd-MM-yyyy"));
    qDebug() << "Date courante : " << m_curentDate;
    this->setImage(this->getImage());
    qDebug() << "Dans le constructeur de Compare";
}

DiffGUI::~DiffGUI()
{
    delete ui;
    qDebug() << "destructeur Compare";
}

void DiffGUI::on_pbOK_clicked()
{
    this->close();

    emit sig_close(0);
}

void DiffGUI::on_pbREPORT_clicked()
{
    m_windowReport = new DiffLogGUI(m_path);
    connect(m_windowReport, &DiffLogGUI::sig_close, this, &DiffGUI::closeWindow);
    m_windowReport->show();
}

void DiffGUI::setImage(QString path)
{
    ui->img2->setPixmap(QPixmap(path));
    ui->img2->setScaledContents(true);

    ui->img1->setPixmap(this->getCurrentImage());
    ui->img1->setScaledContents(true);
}

QString DiffGUI::getImage()
{
    QDirIterator it("/home/pi/Desktop/stockPhotoShare/" + m_cbar + "/", QDir::Dirs, QDirIterator::NoIteratorFlags);
    int val = 0;
    while (it.hasNext()) {
        m_datePath.push_back(it.next());
        m_date.push_back(m_datePath[val].section('/', 6, 6));
        if (m_date[val] == ".." || m_date[val] == ".") {
            m_date[val].clear();
        } //if
        qDebug() << "date origines" << m_date[val];
        val++;
    } //while

    this->clearVoidElement();
    QString finalDate = this->addInitialZero();
    qDebug() << "Date finale : " << finalDate;

    m_date.clear();
    m_datePath.clear();

    return m_DirectoryPath + m_cbar + "/" + finalDate + "/" + m_cbar + "-" + finalDate + "-" + m_faces[m_idFace] + ".jpg";
}

QString DiffGUI::getCurrentImage()
{
    m_path = m_DirectoryPath + m_cbar + "/" + m_curentDate + "/";
    return m_path + m_cbar + "-" + m_curentDate + "-" + m_faces[m_idFace] + ".jpg";
}

void DiffGUI::clearCurrentDate()
{
    for (int i = 0; i < m_date.size(); ++i) {
        //Suppression de la date courante
        if (m_date[i] == m_curentDate) {
            m_date[i] = "";
            qDebug() << "Il y a une date courante identique";
        } //if
    } //for
}

void DiffGUI::clearVoidElement()
{
    this->clearCurrentDate();
    //Supression des élements vides
    for (int i = 0; i < m_date.size(); i++) {
        if (m_date[i] == "" && i < m_date.size() - 1) {
            for (int j = i; j < m_date.size(); j++) {
                if(m_date[i+1] == "" && i < m_date.size() - 2) {
                    if (m_date[i+2] == "" && i < m_date.size() - 3) {
                        m_date[i] = m_date[i+3];
                    } else {
                        m_date[i] = m_date[i+2];
                    }
                } else {
                    m_date[i] = m_date[i+1];
                } //else
            } //for
        } //if
    } //for
    for (int i = 0; i < m_date.size(); i++) {
        qDebug() << "Date clear : " << m_date[i];
    }
    qDebug() << "test ok !! !";
}

QString DiffGUI::compareDate()
{
    int intDate[m_date.size() + 1][3];

    for (int i = 0; i < m_date.size(); i++) {
        for (int j = 0; j < 3; j++) {
            intDate[i][j] = m_date[i].section('-', j, j).toInt(); //Séparation de la date en J M A
        } //for
    } //for

    QString finalDate;
    //Variables tempons
    int lastYear(0);
    int lastMonth(0);
    int lastDay(0);

    qDebug() << "début de la méthode Compare";

    //Algorithme de comparaison des dates
    for (int i = 0; i < m_date.size(); i++) {
        if (i < m_date.size() - 2) {
            //Comparaison de l'année
            if (intDate[i][0] != 0 && intDate[i][1] != 0 && intDate[i][2] != 0) {
                if ((intDate[i][2] < intDate[i+1][2])) {
                    if (lastYear < intDate[i+1][2]) {
                        lastYear = intDate[i+1][2];
                        finalDate = QString::number(intDate[i+1][0]) + "-" + QString::number(intDate[i+1][1]) + "-" + QString::number(intDate[i+1][2]);
                    } //if
                } else if (intDate[i][2] > intDate[i+1][2]) {
                    if (lastYear < intDate[i][2]) {
                        lastYear = intDate[i][2];
                        finalDate = QString::number(intDate[i][0]) + "-" + QString::number(intDate[i][1]) + "-" + QString::number(intDate[i][2]);
                    } //if

                    //Comparaison du mois si les années de deux dates sont identiques
                } else if ((intDate[i][2] == intDate[i+1][2]) && (intDate[i][2] >= lastYear)) {
                    if (intDate[i][1] < intDate[i+1][1]) {
                        if (lastMonth < intDate[i+1][1]) {
                            lastMonth = intDate[i+1][1];
                            finalDate = QString::number(intDate[i+1][0]) + "-" + QString::number(intDate[i+1][1]) + "-" + QString::number(intDate[i+1][2]);
                        } //if
                    } else if (intDate[i][1] > intDate[i+1][1]) {
                        if (lastMonth < intDate[i][1]) {
                            lastMonth = intDate[i][1];
                            finalDate = QString::number(intDate[i][0]) + "-" + QString::number(intDate[i][1]) + "-" + QString::number(intDate[i][2]);
                        } //if

                        //Comparaison des jours si les mois de deux dates sont identiques
                    } else if ((intDate[i][1] == intDate[i+1][1]) && (intDate[i][1] >= lastMonth)) {
                        if (intDate[i][0] < intDate[i+1][0]) {
                            if (lastDay < intDate[i+1][0]) {
                                lastDay = intDate[i+1][0];
                                finalDate = QString::number(intDate[i+1][0]) + "-" + QString::number(intDate[i+1][1]) + "-" + QString::number(intDate[i+1][2]);
                            }

                        } else if (intDate[i][0] > intDate[i+1][0]) {
                            if (lastDay < intDate[i][0]) {
                                lastDay = intDate[i][0];
                                finalDate = QString::number(intDate[i][0]) + "-" + QString::number(intDate[i][1]) + "-" + QString::number(intDate[i][2]);
                            } // if
                        } // else if
                    } // else if
                } //else if
            } //if
        } //if
    } //for
    qDebug() << "LastYear : " << lastYear;
    qDebug() << "LastMonth : " << lastMonth;
    qDebug() << "LastDay : " << lastDay;
    return finalDate;
}

QString DiffGUI::addInitialZero()
{
    qDebug() << "Dans la méthode addInitialZero";
    QString finalDate = this->compareDate();
    QString dateWithoutZero[3];
    for (int i = 0; i < 3; i++) {
        dateWithoutZero[i] = finalDate.section("-", i, i);
        if (dateWithoutZero[i].toInt() < 10) {
            dateWithoutZero[i] = "0" + dateWithoutZero[i];
        }
    }
    return finalDate = dateWithoutZero[0] + "-" + dateWithoutZero[1] + "-" + dateWithoutZero[2];
}


void DiffGUI::on_pbNEXT_clicked()
{
    m_idFace++;
    if(m_idFace > 3) {
        m_idFace = 0;
    }
    this->setImage(this->getImage());
}

void DiffGUI::on_pbPREVIUS_clicked()
{
    m_idFace--;
    if(m_idFace < 0) {
        m_idFace = 3;
    }
    this->setImage(this->getImage());
}

void DiffGUI::closeWindow()
{
   delete m_windowReport;
}
