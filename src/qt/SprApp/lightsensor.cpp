#include <QtGlobal>

#if defined(Q_PROCESSOR_ARM)
#include <bcm2835.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <QDebug>

#include "lightsensor.h"

LightSensor::LightSensor() : _bcm(Bcm2835Wrapper::getInstance())
{
#if defined(Q_PROCESSOR_ARM)
    uint8_t cmd[ 3 ] = {
         0x00    // Command code 0
         , 0x10  // -+
         , 0xc0  // -+-> Gain 1/8 IT 800ms Persistance 1 Int disable Power On
     };

     _bcm.i2c_write(_VEML7700_ADDR, cmd, 3);
#endif
}

LightSensor::~LightSensor()
{
}

/**
 * @brief LightSensor::getAmbientLight
 * @param theLight
 * @return la luminance
 * Valeurs typiques de luminances (source https://www.vishay.com/docs/84323/designingveml7700.pdf)
 *   10^-5lx Light from Sirius, the brightest star in the night sky
 *   10-4lx Total starlight, overcast sky
 *   0.002lx Moonless clear night sky with airglow
 *   0.01lx Quarter moon, 0.27 lx; full moon on a clear night
 *   1lx Full moon overhead at tropical latitudes
 *   3.4lx Dark limit of civil twilight under a clear sky
 *   50lx Family living room
 *   80lx Hallway / bathroom
 *   100lx Very dark overcast day
 *   320lx to 500 lx Office lighting
 *   400lx Sunrise or sunset on a clear day
 *   1000lx Overcast day; typical TV studio lighting
 *   10000lx to 25000lx Full daylight (not direct sun)
 *   32000lx to 130000lx Direct sunlight
 */
int LightSensor::getAmbientLight(double& theLight)
{
    int ret = -1;
#if defined(Q_PROCESSOR_ARM)
    uint8_t cmd = 0x04; // Command code pour lire le registre ALS (Ambient Light Sensor)
    uint8_t buf[ 2 ];

    Bcm2835Wrapper::i2c_err_t  status = _bcm.i2c_write_read_rs(_VEML7700_ADDR, &cmd, 1, buf, 2);
    if( status == Bcm2835Wrapper::SUCCESS) {
        theLight = 0.0576*((buf[ 1 ] << 8) | buf[ 0 ]);
        ret = 0;
    } else {
        qDebug() << "erreur lecture i2c sur VEML7700 : " << status ;
        theLight = 0.0;
    }
#endif

    return ret;
}






