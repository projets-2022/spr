#include "diffloggui.h"
#include "ui_diffloggui.h"

#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QDate>

DiffLogGUI::DiffLogGUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DiffLogGUI)
{
    ui->setupUi(this);
}

DiffLogGUI::DiffLogGUI(QString path) :
    ui(new Ui::DiffLogGUI),
    m_path(path)
{
    ui->setupUi(this);
}

DiffLogGUI::~DiffLogGUI()
{
    delete ui;
    qDebug() << "Destructeur Report";
}

void DiffLogGUI::on_pbCancel_clicked()
{
    this->close();
    //emit sig_close();
    delete this;
}

void DiffLogGUI::on_pbReport_clicked()
{
    if (ui->text->toPlainText() != "") {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Confirm", "are you sure to register ?", QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes) {
            m_text = ui->text->toPlainText();
            QFile file(m_path + "report.txt");
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                return;
            }
            QTextStream out(&file);
            out << QDate::currentDate().toString(QString("dd-MM-yyyy")) << " : " << endl << m_text << endl;
            this->close();
            emit sig_close();
          }
    }
}
