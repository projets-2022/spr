#ifndef SMARTLIGHT_H
#define SMARTLIGHT_H

#include "stripled.h"
#include "lightsensor.h"

class SmartLight : public StripLed
{
public:
    explicit SmartLight();
    // Déclaration nécessaire pour réimporter la méthode masquée (-> name hiding)
    // setBrightness(uint8_t) de la classe de base dans la portée de la classe dérivée
    // Voir https://www.ibm.com/docs/en/zos/2.3.0?topic=udcmco-overloading-member-functions-from-base-derived-classes-c-only
    // ou https://cpp.developpez.com/faq/cpp/?page=Les-classes-en-Cplusplus#Qu-est-ce-que-le-masquage-de-fonction-name-hiding
    using StripLed::setBrightness;
    uint8_t setBrightness(); // éclaire automatiquement selon la luminosité ambiante
    double getLux();

private:
    LightSensor _veml7700;
};

#endif // SMARTLIGHT_H
