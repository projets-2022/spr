#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <QDebug>
#include <QThread>

#include "motor.h"

Motor::Motor()
{
    //instensiation de la classe PCA9629A
    _motor = new PCA9629A;
    //Définition des rampes d'accélération et décélération
    //m_motor->setRamp(PCA9629A::UP, 0x0);
    //m_motor->setRamp(PCA9629A::DOWN, 0x0);
    //Définition de la vitesse
    _motor->setSpeed(PCA9629A::CW, 0xFF); //FF
    _motor->setSpeed(PCA9629A::CCW, 0xFF); //FF
}

Motor::~Motor()
{
    delete _motor;
}

//Tourner à gauche
void Motor::quaterTurnL()
{
    runMotor(0x1C2, LEFT);
}

void Motor::quaterTurnR()
{
    runMotor(0x1C2, RIGHT); // 400/0.222 *1/4 (400 : nb pas moteur, 0.222 rapport mécanique courroie)
}

void Motor::stop()
{
    _motor->stop();
}

bool Motor::isRunning()
{
    return _motor->isRunning();
}

/**
 * @brief Motor::runMotor
 * @param step
 * @param dir
 *
 * Fait tourner le moteur dans un sens ou l'autre
 */
void Motor::runMotor(uint16_t step, dir_t dir)
{
    if (dir == LEFT){
        _motor->setStep(PCA9629A::CCW, step);
        _motor->start(PCA9629A::CCW);
    } else {
        _motor->setStep(PCA9629A::CW, step);
        _motor->start(PCA9629A::CW);
    }

    //Boucle infinie -> tant que le moteur tourne
    while(1) {
        if (_motor->isRunning()) {
            QThread::sleep(1);
        } else {
            break;
        }

    }
}
