#ifndef LIGHTSENSOR_H
#define LIGHTSENSOR_H

#include <QObject>
#include "bcm2835wrapper.h"

#define MODE_READ 0
#define MODE_WRITE 1

#define MAX_LEN 32

/*
 * Classe LightSensor
 * Gère le capteur de luminosité VEML7700 via une communication I2C
 * Utilise la classe Bcm2835Wrapper (BCM2835)
 */

class LightSensor
{
public:
    explicit LightSensor();
    ~LightSensor();

    int getAmbientLight(double& theLight);

private:
     Bcm2835Wrapper& _bcm;
     const uint8_t _VEML7700_ADDR = 0x10;
};

#endif // LIGHTSENSOR_H
