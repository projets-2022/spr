#ifndef MOTOR_H
#define MOTOR_H

#include "pca9629a.h"

/*
 * Classe Motor
 * Gère la rotation du moteur pour faire un quart de tour
 * Utilise la classe PCA9629A
 */

class Motor : public QObject
{
    Q_OBJECT

public:
    Motor();
    ~Motor();

    void quaterTurnL();
    void quaterTurnR();
    void stop();
    bool isRunning();

private:
    typedef enum {
        RIGHT,
        LEFT
    } dir_t;

    PCA9629A * _motor;
    void runMotor(uint16_t step = 0x64, dir_t dir = RIGHT);

signals:
};

#endif // MOTOR_H
