#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include <QProcess>
#include <QMainWindow>
#include <QtGlobal>

#include "bcm2835wrapper.h"

class Camera : public QObject
{
Q_OBJECT

public:
    typedef enum : char {      // enum pour indiquer la face de la photo
        FRONT = 'F',
        RIGHT = 'R',
        BACK = 'B',
        LEFT = 'L',
    } face_t;

    // Enuméré qui indique sur quel port de la carte multicam sont branchées les caméras grand angle (-> WIDE)
    // et angle reserré (-> NARROW)
    typedef enum : char {
        NARROW = 'C',
        WIDE = 'A'
    } cam_t;

    Camera();
    ~Camera();

    QString takeCapture(face_t side);
    QImage getCapture(face_t side);
    void delAllCaptures();
    void archiveCaptures(QString folderName, QString changeLog);
    void init();
    void select(cam_t cam);

private:
#if defined(Q_PROCESSOR_ARM)
    const QString _TEMP_PATH = "/dev/shm/"; //chemin de stockage temporaire
    const QString _ARCHIVE_BASE_PATH = "/dev/shm/"; //chemin de stockage temporaire
#else
    const QString _TEMP_PATH = "c:\\tmp\\"; //chemin de stockage temporaire
    const QString _ARCHIVE_BASE_PATH = "c:\\tmp\\archive\\"; //chemin de stockage temporaire
#endif
    const QString _CAPTURE_PREFIX = "face"; // préfixe de nom de fichier pour chaque capture

    QImage _captures[ 4 ]; // captures de la caméra pour chaque face

    Bcm2835Wrapper &_bcm;

    // Structure et Hash qui définissent pour chaque caméra de la carte multicam :
    // * l'état à fixer sur les broches 4, 17, 18
    // * la valeur à envoyer sur l'esclave i2c
    // pour la sélectionner
    struct _cfg {
        uint8_t i2cSetValue;
        bool isPin4High;
        bool isPin17High;
        bool isPin18High;
    };
    QHash<char, struct _cfg> _setup = {
        {'A', {0x04, false, false, true}}
        , {'B', {0x05, true, false, true}}
        , {'C', {0x06, false, true, false}}
        , {'D', {0x07, true, true, false}}
    };


};

#endif // CAMERA_H
