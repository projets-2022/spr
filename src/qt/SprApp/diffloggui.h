#ifndef REPORTTEXT_H
#define REPORTTEXT_H

#include <QMainWindow>

namespace Ui {
class DiffLogGUI;
}

class DiffLogGUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit DiffLogGUI(QWidget *parent = nullptr);
    DiffLogGUI(QString);
    ~DiffLogGUI();

private:
    Ui::DiffLogGUI *ui;
    QString m_text;
    QString m_path;

signals:
    void sig_close();

private slots:
    void on_pbCancel_clicked();
    void on_pbReport_clicked();
};

#endif // REPORTTEXT_H
