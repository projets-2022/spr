#ifndef MAINGUI_H
#define MAINGUI_H

#include <QWidget>
#include <QLabel>
#include <QPair>
#include <QTimer>
#include <QProgressDialog>

#include "logingui.h"
#include "camera.h"
#include "clickablelabel.h"
#include "smartlight.h"
#include "motor.h"
#include "hcsr04.h"
#include "fakehcsr04.h"
#include "volumedetectiongui.h"

namespace Ui {
class MainGUI;
}

class MainGUI : public QWidget
{
    Q_OBJECT

public:
    explicit MainGUI(QWidget *parent = nullptr);
    ~MainGUI();

private:
    Ui::MainGUI *ui;

    LoginGUI * _login;
    Camera * _cam;
    Motor _motor;
    SmartLight _light;
    VolumeSensor * _volume;
    QTimer * _volumeDetectionTimer;
    VolumeDetectionGUI * _waitVolumeDetectionDlg;

    QVector<QPair<Camera::face_t, ClickableLabel *>> _faces;

private slots:
    void onLoginSubmit(QString userName, QString password);
    void onCameraModeChanged();
    void onLightModeChanged();
    void onMotorModeChanged();
    void onBtnSwitchCameraClicked();
    void onBtnShootClicked();
    void onFaceClicked();
    void on_sldrBrightness_valueChanged(int value);
    void on_btnTurnLeft_clicked();
    void on_btnTurnRight_clicked();
    void on_btnArchive_clicked();
    void on_btnLightAuto_clicked();
    void on_btnQuit_clicked();
    void onVolumeDetectionCanceled();
    void onVolumeDetectionQuit();
};

#endif // MAINGUI_H
