#include <QDebug>

#include "smartlight.h"

SmartLight::SmartLight() : StripLed()
{
}

uint8_t SmartLight::setBrightness()
{
    double als;
    long lux;
    uint8_t brightness;

    _veml7700.getAmbientLight(als);
    lux = static_cast<long>(als);

    if((lux > 0) && (lux < 30)) {
        brightness = 100;
    } else if((lux > 30) && (lux < 70)) {
        brightness = 75;
    } else if((lux > 30) && (lux < 70)) {
        brightness = 75;
    } else if((lux > 70) && (lux < 100)) {
        brightness = 50;
    } else if((lux > 100) && (lux < 200)) {
        brightness = 33;
    } else if((lux > 200) && (lux < 500)) {
        brightness = 25;
    } else if((lux > 500) && (lux < 2000)) {
        brightness = 10;
    } else {
        brightness = 0;
    }

    setBrightness(brightness);

    return brightness;
}

double SmartLight::getLux()
{
    double als;
    _veml7700.getAmbientLight(als);
    return als;
}
