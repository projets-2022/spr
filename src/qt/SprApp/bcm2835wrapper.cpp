#include <iostream>

#include "bcm2835wrapper.h"

using namespace std;

Bcm2835Wrapper &Bcm2835Wrapper::getInstance()
{
    static Bcm2835Wrapper instance;
    return instance;
}

/**
 * Retourne la fréquence du coeur du cpu (en MHz) utilisée pour piloter
 * les horloges des périphériques internes (i2c, spi).
 * 250 pour les RPI1&2, 400 pour les RPi3, 500 pour les RPi4
 */
uint16_t Bcm2835Wrapper::_getCoreClkMHz() {
    const char cmd[] = "vcgencmd measure_clock core";
    uint32_t clockHz = 0;

    char buf[ 64 ];

    FILE *fp = popen(cmd, "r");

    if (fgets(buf, sizeof(buf)-1, fp) != 0) {
        sscanf(buf, "%*[^=]=%d", &clockHz);
    }

    fclose(fp);

    return (uint16_t)(clockHz / 1e6);
}


Bcm2835Wrapper::Bcm2835Wrapper()
{
#if defined(Q_PROCESSOR_ARM)
    if(bcm2835_init() != 1) {
        cout << "Erreur initialisation librairie bcm2835" << endl;
    } else {
        if( (bcm2835_i2c_begin() != 1) || (bcm2835_spi_begin() != 1)) {
            cout << "Erreur initialisation bus I2C librairie bcm2835. Programme exécuté sans `sudo` ?" << endl;
        } else {
            _coreFreqMHz = _getCoreClkMHz();
            i2c_setClock(STANDARD);
            spi_setClock(SPI_SCLK_100KHZ);
        }
    }
#endif
}

Bcm2835Wrapper::~Bcm2835Wrapper()
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_i2c_end();
    bcm2835_spi_end();
    bcm2835_close();
#endif
}


void Bcm2835Wrapper::gpio_fsel(uint8_t pin, gpio_mode_t mode) {
#if defined(Q_PROCESSOR_ARM)
    uint8_t dir;

    switch(mode) {
    case Bcm2835Wrapper::INPUT :
        dir = BCM2835_GPIO_FSEL_INPT;
        break;
    case Bcm2835Wrapper::OUTPUT :
        dir = BCM2835_GPIO_FSEL_OUTP;
        break;
    default:
        dir = -1;
        break;
    }

    if(dir > 0) {
        bcm2835_gpio_fsel(pin, dir);
    }
#endif
}

void Bcm2835Wrapper::gpio_set_pud(uint8_t pin, gpio_pud_t pud) {
#if defined(Q_PROCESSOR_ARM)
    int pudCtrl;

    switch(pud) {
    case Bcm2835Wrapper::OFF :
        pudCtrl =  BCM2835_GPIO_PUD_OFF;
        break;
    case Bcm2835Wrapper::DOWN :
        pudCtrl =  BCM2835_GPIO_PUD_DOWN;
        break;
    case Bcm2835Wrapper::UP :
        pudCtrl =  BCM2835_GPIO_PUD_UP;
        break;
    default:
        pudCtrl = -1;
        break;
    }

    if(pudCtrl > 0) {
        bcm2835_gpio_set_pud(pin, pudCtrl);
    }
#endif
}

void Bcm2835Wrapper::gpio_set(uint8_t pin) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_gpio_set(pin);
#endif
}

void Bcm2835Wrapper::gpio_clr(uint8_t pin) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_gpio_clr(pin);
#endif
}

uint8_t Bcm2835Wrapper::gpio_lev(uint8_t pin) {
#if defined(Q_PROCESSOR_ARM)
    return bcm2835_gpio_lev(pin);
#endif
}

void Bcm2835Wrapper::i2c_setClock(Bcm2835Wrapper::i2c_clock_t clock)
{
#if defined(Q_PROCESSOR_ARM)
    uint32_t cdiv[3][3] = {
        {2500, 626, 250}
        , {4000, 1000, 400}
        , {5000, 1250, 500}
    };
    int idx;
    uint32_t clockDivider;

    switch(_coreFreqMHz) {
        case 250 :
            idx = 0;
            break;
        case 400 :
            idx = 1;
            break;
        case 500 :
            idx = 2;
            break;
        default :
            idx = 0;
            break;
    };

    switch(clock) {
    case Bcm2835Wrapper::STANDARD :
        clockDivider = cdiv[idx][0];
        break;
    case Bcm2835Wrapper::FULL :
        clockDivider = cdiv[idx][1];
        break;
    case Bcm2835Wrapper::FAST :
        clockDivider = cdiv[idx][2];
        break;
    default:
        clockDivider = cdiv[idx][0];
        break;
    }

    bcm2835_i2c_setClockDivider(clockDivider);
#endif
}

Bcm2835Wrapper::i2c_err_t Bcm2835Wrapper::i2c_read(uint8_t addr, uint8_t *buf, int len)
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_i2c_setSlaveAddress(addr);

    i2c_err_t status = static_cast<i2c_err_t>(bcm2835_i2c_read(reinterpret_cast<char*>(buf), len));

    return status;
#endif
}

Bcm2835Wrapper::i2c_err_t Bcm2835Wrapper::i2c_write_read_rs(uint8_t addr, uint8_t *cmd, int cmdLen, uint8_t *buf, int bufLen)
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_i2c_setSlaveAddress(addr);

    i2c_err_t status = static_cast<i2c_err_t>(bcm2835_i2c_write_read_rs(
        reinterpret_cast<char*>(cmd)
        , cmdLen
        , reinterpret_cast<char*>(buf)
        , bufLen
        ));

    return status;

#endif
}

Bcm2835Wrapper::i2c_err_t Bcm2835Wrapper::i2c_write(uint8_t addr, uint8_t* buf, int len) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_i2c_setSlaveAddress(addr);

    i2c_err_t status = static_cast<i2c_err_t>(bcm2835_i2c_write(reinterpret_cast<char*>(buf), len));

    return status;
#endif
}

void Bcm2835Wrapper::spi_setClock(spi_clock_t clock) {
#if defined(Q_PROCESSOR_ARM)
    uint16_t cdiv[3][3] = {
        {2500, 250}    // core_freq=250MHz => RPI1
        , {4000, 400} // core_freq=400MHz => RPi3
        , {5000, 500} // core_freq=500MHz => RPi4
    };
    int idx;
    uint16_t clockDivider;

    switch(_coreFreqMHz) {
        case 250 :
            idx = 0;
            break;
        case 400 :
            idx = 1;
            break;
        case 500 :
            idx = 2;
            break;
        default :
            idx = 0;
            break;
    };

    switch(clock) {
    case Bcm2835Wrapper::SPI_SCLK_100KHZ :
        clockDivider = cdiv[idx][0];
        break;
    case Bcm2835Wrapper::SPI_SCLK_1MHz :
        clockDivider = cdiv[idx][1];
        break;
    default:
        clockDivider = cdiv[idx][0];
        break;
    }

    bcm2835_spi_setClockDivider(clockDivider);
#endif
}

void Bcm2835Wrapper::spi_setDataMode(spi_mode_t mode) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_spi_setDataMode(mode);
#endif
}

void Bcm2835Wrapper::spi_chipSelect(spi_cs_t cs) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_spi_chipSelect(cs);
#endif
}

void Bcm2835Wrapper::spi_setChipSelectPolarity(spi_cs_t cs, level_t activeLevel) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_spi_setChipSelectPolarity(cs, activeLevel);
#endif
}

void Bcm2835Wrapper::spi_writenb(uint8_t * tbuf, int len) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_spi_writenb(reinterpret_cast<const char*>(tbuf), len);
#endif
}

void Bcm2835Wrapper::spi_transfernb(uint8_t * tbuf, uint8_t * rbuf, int len) {
#if defined(Q_PROCESSOR_ARM)
    bcm2835_spi_transfernb(reinterpret_cast<char *>(tbuf)
    , reinterpret_cast<char *>(rbuf)
    , len
    );
#endif
}

void Bcm2835Wrapper::pwm_set_clock(uint32_t divisor)
{
#if defined(Q_PROCESSOR_ARM)
   bcm2835_pwm_set_clock (divisor);
#endif
}

void Bcm2835Wrapper::pwm_set_mode(uint8_t channel, uint8_t markspace, uint8_t enabled)
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_pwm_set_mode (channel, markspace, enabled);
#endif
}

void Bcm2835Wrapper::pwm_set_range(uint8_t channel, uint32_t range)
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_pwm_set_range (channel, range);
#endif
}

void Bcm2835Wrapper::pwm_set_data(uint8_t channel, uint32_t data)
{
#if defined(Q_PROCESSOR_ARM)
    bcm2835_pwm_set_data (channel, data);
#endif
}
