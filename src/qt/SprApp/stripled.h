#ifndef STRIPLED_H
#define STRIPLED_H

#include "bcm2835wrapper.h"
#include "lightsensor.h"

/*
 * Classe StripLed
 * Pilote le ruban de dels en PWM avec la librairie BCM2835
 */

class StripLed
{
public:
    StripLed();
    ~StripLed();

    virtual void setBrightness(uint8_t level);

private:
    const int _PWM_PIN = RPI_V2_GPIO_P1_32;
    const int _PWM_CHANNEL = 0;
    const int _PWM_RANGE = 1024;
};
#endif // STRIPLED_H
