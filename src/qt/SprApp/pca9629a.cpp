#include <stdio.h>
#include <unistd.h>
#include <QDebug>

#include <iostream>

using namespace std;

#include "pca9629a.h"

PCA9629A::PCA9629A(uint8_t addr) : m_addr(addr), _bcm(Bcm2835Wrapper::getInstance()), m_isRunning(false) {
    Reset();

    initRegs();
}

PCA9629A::~PCA9629A() {
    qDebug() << "Destructeur PCA9629A";
}

void PCA9629A::Reset() {
    uint8_t cmd[] = "\x06";

    _bcm.i2c_write(0x00, cmd, 1);

    usleep(1200); // Durée soft reset : 1.2ms (voir datasheet)
}

void PCA9629A::initRegs() {
    uint8_t cmd[] = {
        0x80, //  register access start address (0x00) with incremental access flag (MSB)
        0x20, 0x0A, 0x00, 0x03, 0x13, 0x1C, //  for registers MODE - MSK (0x00 - 0x07
        0x00, 0x00, 0x68, 0x00, 0x00, //  for registers INTSTAT - EXTRASTEPS1 (0x06, 0xA)
        0x10, 0x80, //  for registers OP_CFG_PHS and OP_STAT_TO (0x0B - 0xC)
        0x09, 0x09, 0x01, 0x7D, 0x7D, //  for registers RUCNTL - LOOPDLY_CCW (0xD- 0x10)
        0xFF, 0x01, 0xFF, 0x01, 0x05, 0x0D, 0x05, 0x0D, //  for registers CWSCOUNTL - MCNTL (0x12 - 0x1A)
        0x20, //  for register MCNTL (0x1A)
        0xE2, 0xE4, 0xE6, 0xE0 //  for registers SUBADR1 - ALLCALLADR (0x1B - 0x1E)
    };

    _bcm.i2c_write(m_addr, cmd, sizeof (cmd) / sizeof (uint8_t));
}

void PCA9629A::write(reg8 reg, uint8_t val) {
    uint8_t cmd[ 2 ];

    cmd[ 0 ] = reg;
    cmd[ 1 ] = val;

    _bcm.i2c_write(m_addr, cmd, 2);
}

void PCA9629A::write(reg16 reg, uint16_t val) {
    uint8_t cmd[ 3 ];

    cmd[ 0 ] = reg;
    cmd[ 1 ] = val & 0x00ff;
    cmd[ 2 ] = val >> 8;

    _bcm.i2c_write(m_addr, cmd, 3);
}

uint8_t PCA9629A::read(reg8 reg) {
    uint8_t val;
    uint8_t cmd = (uint8_t)reg;

    // Sélectionner le registre à lire
    _bcm.i2c_write(m_addr, &cmd, 1);

    // Lire le registre
    _bcm.i2c_read(m_addr, &val, 1);

    return val;
}

uint16_t PCA9629A::read(reg16 reg) {
    uint8_t val[ 2 ];
    uint8_t cmd = (uint8_t)reg;

    // Sélectionner le registre à lire
    _bcm.i2c_write(m_addr, &cmd, 1);

    // Lire le registre
    _bcm.i2c_read(m_addr, val, 2);

    return *((uint16_t *)val);
}

uint32_t PCA9629A::read(reg32 reg) {
    uint8_t val[ 4 ];
    uint8_t cmd = (uint8_t)reg;

    // Sélectionner le registre à lire
    _bcm.i2c_write(m_addr, &cmd, 1);

    // Lire le registre
    _bcm.i2c_read(m_addr, val, 4);

    return *((uint32_t *)val);
}

void PCA9629A::start(dir dir) {
    write(MCNTL, 0x80 | dir);
}

void PCA9629A::stop() {
    write(MCNTL, 0xA0);
}

void PCA9629A::setStep(dir dir, int stepCount) {
    write((dir == CW) ? CWSCOUNT : CCWSCOUNT, stepCount);
}

uint16_t PCA9629A::setSpeed(dir dir, int pulsePerSecond) {
    uint8_t prescaler = 0;
    uint8_t ratio;

    ratio = (1.0/24.576e-3) / pulsePerSecond;

    prescaler = (ratio & 0x01) ? 1 : prescaler;
    prescaler = (ratio & 0x02) ? 2 : prescaler;
    prescaler = (ratio & 0x04) ? 3 : prescaler;
    prescaler = (ratio & 0x08) ? 4 : prescaler;
    prescaler = (ratio & 0x10) ? 5 : prescaler;
    prescaler = (ratio & 0x20) ? 6 : prescaler;
    prescaler = (ratio & 0x40) ? 7 : prescaler;

    uint16_t stepPulseWidth;

    stepPulseWidth = STEP_RESOLUTION / ((1 << prescaler) * pulsePerSecond);

    if (stepPulseWidth & 0xE000) { //error( "pps setting: out of range" );
        stepPulseWidth = 0x1FFF;
        printf("the pps forced in to the range that user specified.. %fpps\r\n", (float) STEP_RESOLUTION / ((float) 0x1FFF * (float) (1 << prescaler)));
    } //if
    if (!stepPulseWidth) { //error( "pps setting: out of range" );
        stepPulseWidth = 0x1;
        printf("the pps forced in to the range that user specified.. %fpps\r\n", (float) STEP_RESOLUTION / (float) (1 << prescaler));
    } //if

    stepPulseWidth |= (prescaler << 13);
    write((dir == CW) ? CWPW : CCWPW, stepPulseWidth);

    return ( stepPulseWidth);
}

void PCA9629A::setRamp(ramp ramp, uint8_t slope) {
    uint8_t cmd = 0x20;

    cmd |= slope ;

    write((ramp == UP) ? RUCNTL : RDCNTL, cmd);
}

bool PCA9629A::isRunning() {
    uint8_t mcntl = read(MCNTL);

    return (mcntl & 0x80) ? true : false;
}

uint32_t PCA9629A::getStepNumber() {
    return read(STEPCOUNT);
}
