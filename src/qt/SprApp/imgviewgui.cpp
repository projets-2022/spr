#include <QDebug>

#include <QDebug>

#include "imgviewgui.h"
#include "ui_imgviewgui.h"

ImgViewGUI::ImgViewGUI(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImgViewGUI)
{
    ui->setupUi(this);
}

ImgViewGUI::ImgViewGUI(QImage& image, int idImage) :
    ui(new Ui::ImgViewGUI),
    m_idImage(idImage)
{
    ui->setupUi(this);
    setModal(true);
    setWindowFlag(Qt::WindowStaysOnTopHint);

    ui->label->setPixmap(QPixmap::fromImage(image));
    ui->label->setScaledContents(true);
}

ImgViewGUI::~ImgViewGUI()
{
    delete ui;
    qDebug() << "Destructeur viewImage";
}

void ImgViewGUI::on_pbOk_clicked()
{
    this->close();
    delete this;
    //emit sig_close(1);
}

void ImgViewGUI::on_pbCompare_clicked()
{
    emit sig_compare(m_idImage);
}
