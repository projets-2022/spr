#include "camera.h"
#include <QDebug>
#include <QImageReader>
#include <unistd.h>
#include <QDate>
#include <QDebug>
#include <QDirIterator>

Camera::Camera() :
    _bcm(Bcm2835Wrapper::getInstance())
{
    init();
}

Camera::~Camera()
{
}


QString Camera::takeCapture(face_t face)
{
    QString outputFile = _TEMP_PATH + _CAPTURE_PREFIX + face + ".jpg";
    QProcess raspistillProcess;

    QStringList args;
    args.append("-n");  // pas de prévisualisation
    args.append("-t");  //-+-> timeout 1ms avant de prendre la photo
    args.append("1");   //-+
    args.append("-md");
    args.append("1");
    args.append("-o");      //-+-> fichier de sortie (-> /dev/shm/face[FBRL].jpg)
    args.append(outputFile);//-+

    QString str;
    str.append(_TEMP_PATH);
    str.append(_CAPTURE_PREFIX);
    str.append(face);
    str.append(".jpg");
    qDebug() << "[" << __BASE_FILE__ << "] Taking shot : " << str;

    raspistillProcess.execute("raspistill", args);

    return outputFile;
}

QImage Camera::getCapture(face_t side)
{
    QImageReader reader(_TEMP_PATH + _CAPTURE_PREFIX + side + ".jpg");
    reader.setAutoTransform(true);

    return reader.read();
}

void Camera::delAllCaptures()
{
    /*
    QProcess rmProcess;
    QString cmd;
    cmd = "rm " + _TEMP_PATH + _CAPTURE_PREFIX + "*.jpg";

    // rmProcess.startDetached(cmd);
    rmProcess.execute(cmd);
    */

    // Détruire tous les fichiers du disque virtuel
    // (Source : https://stackoverflow.com/questions/52133207/qt-c-remove-file-with-name-contains)
    QDirIterator it(_TEMP_PATH, {_CAPTURE_PREFIX+"*.jpg"});

    while (it.hasNext())
        QFile(it.next()).remove();
}

void Camera::archiveCaptures(QString folderName, QString changeLog)
{
#ifdef A_REVOIR
    for (face_t i = FRONT; i < Camera::BACK+1; i++) {
        qDebug() << i;
        _captures[i] = getCapture(i);
    }

    QString date = QDate::currentDate().toString(QString("dd-MM-yyyy"));
    for (int i = 0; i < 4; i++) {
        QString mkdir = "sudo mkdir /home/pi/Desktop/stockPhotoShare/" + barCode +"/";
        _createFolderProcess->setProgram(mkdir);
        _createFolderProcess->execute(mkdir);
        mkdir = "sudo mkdir /home/pi/Desktop/stockPhotoShare/" + barCode +"/" + date + "/";
        _createFolderProcess->setProgram(mkdir);
        _createFolderProcess->execute(mkdir);
        _captures[i].save("/home/pi/Desktop/stockPhotoShare/"+ barCode + "/" + date + "/" + barCode + "-" + date + "-" + _idSide[i] + ".jpg");
    }
#endif
    QDir archiveFolder;

    QString date = QDate::currentDate().toString(QString("dd-MM-yyyy"));
    archiveFolder.root();
    archiveFolder.mkpath(_ARCHIVE_BASE_PATH + folderName + QDir::separator() + date);
    archiveFolder.cd(_ARCHIVE_BASE_PATH + folderName + QDir::separator() + date);

    QDirIterator it(_TEMP_PATH, {_CAPTURE_PREFIX+"*.jpg"});

    while (it.hasNext()) {
        QFile f(it.next());
        QString str = archiveFolder.path() + QDir::separator() + QFileInfo(f).fileName();
        qDebug() << "backup : " << str;
        f.copy(str);
    }
}

void Camera::init()
{
    _bcm.gpio_fsel(4, Bcm2835Wrapper::OUTPUT);
    _bcm.gpio_fsel(17, Bcm2835Wrapper::OUTPUT);
    _bcm.gpio_fsel(18, Bcm2835Wrapper::OUTPUT);
}

void Camera::select(cam_t cam)
{
    // Récupère la config spécifique à la caméra spécifiée dans le paramètre `cam`
    struct _cfg config = _setup.value(cam);

    qDebug() << "Caméra sélectionnée :" << cam;

    // Applique la config
    _bcm.i2c_write(0x70, &config.i2cSetValue, 1);
    config.isPin4High ? _bcm.gpio_set(4) : _bcm.gpio_clr(4);
    config.isPin17High ? _bcm.gpio_set(17) : _bcm.gpio_clr(17);
    config.isPin18High ? _bcm.gpio_set(18) : _bcm.gpio_clr(18);
}



