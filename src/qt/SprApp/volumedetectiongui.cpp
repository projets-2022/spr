#include "volumedetectiongui.h"
#include "ui_volumedetectiongui.h"

VolumeDetectionGUI::VolumeDetectionGUI(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::VolumeDetectionGUI)
{
    ui->setupUi(this);

    setWindowFlags(Qt::Window | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowModality(Qt::WindowModal);

    ui->progressBar->setValue(0);

    connect(&tmr, &QTimer::timeout, this, &VolumeDetectionGUI::onTmrTimeout);
    tmr.start(500);
}

VolumeDetectionGUI::~VolumeDetectionGUI()
{
    delete ui;
}

void VolumeDetectionGUI::on_btnCancel_clicked()
{
    tmr.stop();
    emit canceled();
}

void VolumeDetectionGUI::onTmrTimeout()
{
    static int progress = 0;

    progress += 5;

    ui->progressBar->setValue(progress);

    if(progress >= 100) {
        tmr.stop();
        progress = 0;
        emit quit();
    }
}

